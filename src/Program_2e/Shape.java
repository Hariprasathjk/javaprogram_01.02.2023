package Program_2e;

public abstract class Shape {
	abstract void RectangleArea(int length, int breadth);

	abstract void SquareArea(int side);

	abstract void CircleArea(int radius);
}
