package program_2f;

import java.util.*;

public class LinkedListD {
	public static void main(String args[]) {

		LinkedList<String> ll = new LinkedList<String>();

		ll.add("Milk");
		ll.add("Coffee");
		ll.add("Tea");
		ll.add("Coke");
		ll.add("Juice");

		System.out.println("Size of the list: " + ll.size());

		System.out.println("Contents of the list: " + ll);

		ll.remove(1);

		if (ll.contains("Coke")) {
			System.out.println("Coke is present in the list");
		} else {
			System.out.println("Coke is not present in the list");
		}
	}
}
