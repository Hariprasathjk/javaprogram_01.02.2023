package program_2f;

import java.util.ArrayList;

public class ArrayListExample {

	public static void main(String[] args) {

		ArrayList<String> products = new ArrayList<>();

		products.add("Pepsi");
		products.add("Coke");
		products.add("Fanta");
		products.add("Sprite");
		products.add("Thums Up");

		System.out.println("Size of list = " + products.size());

		System.out.println("Contents of list = " + products);

		products.remove(1);

		if (products.contains("Coke")) {
			System.out.println("Coke is present in the list");
		} else {
			System.out.println("Coke is not present in the list");
		}
	}
}
