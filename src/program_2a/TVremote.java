package program_2a;

public interface TVremote {

	public void powerOn();

	public void powerOff();

	public void volumeUp();

	public void volumeDown();
}
