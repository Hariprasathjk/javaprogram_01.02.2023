package program_2a;

public interface SmartTVremote extends TVremote {
	public void changeChannel();

	public void pause();

	public void play();

}
