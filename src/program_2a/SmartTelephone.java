package program_2a;

public class SmartTelephone extends Telephone {
	@Override
	public void lift() {
		System.out.println("Lifting the Smart Telephone");
	}

	@Override
	public void disconnected() {
		System.out.println("Disconnecting the Smart Telephone");
	}
}
