package program_2a;

public abstract class TV implements SmartTVremote {
    @Override
    public void powerOn() {
        System.out.println("Powering on the TV");
    }

    @Override
    public void powerOff() {
        System.out.println("Powering off the TV");
    }

    @Override
    public void volumeUp() {
        System.out.println("Increasing the volume of the TV");
    }
   

}