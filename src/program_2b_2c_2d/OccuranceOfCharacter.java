package program_2b_2c_2d;

public class OccuranceOfCharacter {

	public static void main(String[] args) {
		String str = "Hello, World";
		System.out.println("First occurrence of 'o' : " + str.indexOf('o'));
		System.out.println("Last occurrence of 'o' : " + str.lastIndexOf('o'));
		System.out.println("First occurrence of ',' : " + str.indexOf(','));
		System.out.println("Last occurrence of ',' : " + str.lastIndexOf(','));
	}
}
