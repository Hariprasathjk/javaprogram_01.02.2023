package program_1d;


	public class SportsCar extends Car { 
	    private int maxSpeed; 
	  
	    public SportsCar(String color, String brand, int maxSpeed) 
	    { 
	        super(color, brand); 
	        this.maxSpeed = maxSpeed; 
	    } 
	  
	    public void displayCar() 
	    { 
	        System.out.println("Brand: "+brand+" Color: "+color+" MaxSpeed: "+maxSpeed); 
	    } 
	    
	   public static void main(String[] args) {
		Car c=new SportsCar("green", "Audi", 200);
		c.displayCar();
	}
	} 

