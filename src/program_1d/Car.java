package program_1d;

public class Car {

	protected String color;
	protected String brand;

	public Car(String color, String brand) {
		this.color = color;
		this.brand = brand;
	}

	public void displayCar() {
		System.out.println("Brand: " + brand + " Color: " + color);
	}
}
